package main

import (
	"log"
	"pustaka-api/book"
	"pustaka-api/handler"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)



func main() {

	dsn := "root:@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("Db connection error")
	}

	db.AutoMigrate(&book.Book{})

	bookRepository := book.NewRepository(db)
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)
	
	router := gin.Default()

	v1 := router.Group("/v1")

	v1.GET("/books", bookHandler.GetBooks)
	v1.GET("/books/:id", bookHandler.GetBook)
	v1.POST("/books", bookHandler.CreateBook)
	v1.PUT("/books/:id", bookHandler.UpdateBook)
	v1.DELETE("/books/:id", bookHandler.DeleteBook)

	router.Run()
}




	// CREATE
	// book := book.Book{}
	// book.Title = "Atomic habits"
	// book.Price = 120000
	// book.Rating = 4
	// book.Discount = 15
	// book.Description = "Buku self development."

	// err = db.Create(&book).Error
	// if err != nil {
	// 	fmt.Println("=======================")
	// 	fmt.Println("Error creating book record")
	// 	fmt.Println("=======================")
	// }

	// READ
	// GET All
	// var books []book.Book
	// err = db.Debug().Find(&books).Error
	// if err != nil {
	// 	fmt.Println("=======================")
	// 	fmt.Println("Error finding all book record")
	// 	fmt.Println("=======================")
	// }

	// for _, b := range books {
	// 	fmt.Println("Title :", b.Title)
	// 	fmt.Printf("Book Object %v", b)
	// }

	// // GET by Id
	// var book book.Book
	// err = db.Debug().First(&book, 2).Error
	// if err != nil {
	// 	fmt.Println("=======================")
	// 	fmt.Println("Error finding book record")
	// 	fmt.Println("=======================")
	// }

	// fmt.Println("Title :", book.Title)
	// fmt.Printf("Book object %v", book)

	// //GET by String Conditions
	// err = db.Debug().Where("title = ?", "Man Tiger").Find(&books).Error
	// if err != nil {
	// 	fmt.Println("=======================")
	// 	fmt.Println("Error finding all book record")
	// 	fmt.Println("=======================")
	// }

	// for _, b := range books {
	// 	fmt.Println("Title :", b.Title)
	// 	fmt.Printf("Book Object %v", b)
	// }


	// UPDATE

	// var book book.Book
	// err = db.Debug().Where("id = ?", 1).First(&book).Error
	// if err != nil {
	// 	fmt.Println("=======================")
	// 	fmt.Println("Error finding book record")
	// 	fmt.Println("=======================")
	// }

	// book.Title = "Man Tiger (Revised edition)"
	// err = db.Save(&book).Error
	// if err != nil {
	// 	fmt.Println("=======================")
	// 	fmt.Println("Error updating book record")
	// 	fmt.Println("=======================")
	// }

	// DELETE
	
	// var book book.Book
	// err = db.Debug().Where("id = ?", 1).First(&book).Error
	// if err != nil {
	// 	fmt.Println("=======================")
	// 	fmt.Println("Error finding book record")
	// 	fmt.Println("=======================")
	// }

	// err = db.Delete(&book).Error
	// if err != nil {
	// 	fmt.Println("=======================")
	// 	fmt.Println("Error deleting book record")
	// 	fmt.Println("=======================")
	// }